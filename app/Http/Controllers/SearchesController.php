<?php

namespace App\Http\Controllers;

use Config;

use App\Search;
use App\Place;
use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;
use Maatwebsite\Excel\Facades\Excel;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Cornford\Googlmapper\Models\Map as Map;

class SearchesController extends Controller
{
    public function run(Search $search) {
        $search-> run();
    }

    public function pause(Search $search) {
        $search-> pause();
        return Redirect('/home')->with('msg', 'The search is now paused');
    }

    public function create()
    {
        return view('searches.create');
    }

    public function resultsByPlaces(Search $search)
    {
        $xls_name = $search-> name;

        Excel::create($xls_name, function($excel) use ($search) {

            foreach (explode(',' , $search-> keywords) as $keyword) {

                $excel->sheet($keyword, function($sheet) use($search, $keyword) {
                    $places = $search-> places()->where('item', '=', $keyword)-> get();

                    $results = array();

                    foreach ($places as $place){
                        $contacts = $place-> contacts()-> get();

                        $contacts_string = "";

                        foreach ($contacts as $contact){
                            $contacts_string .= $contact-> email.', ';
                        }

                        $contacts_string = substr($contacts_string, 0 , -2);

                        $result = array( 'Nombre' => $place-> name,
                                            'País' => $place-> country,
                                            'Localidad' => $place-> locality,
                                            'Zona' => $place-> administrative_area,
                                            'CP' => $place-> postal_code,
                                            'Dirección' => $place-> street_name . ' ' . $place-> street_number,
                                            'Teléfono' => $place-> phone_number,
                                            'Latitude' => $place-> latitude,
                                            'Longitude' => $place-> longitude,
                                            'Website' => $place-> website,
                                            'Emails' => $contacts_string);

                        array_push($results, $result);
                    }

                    $sheet-> fromArray($results);
                });
            }

        })->download('xls');
    }

    public function resultsByContacts()
    {
        $xls_name = 'Todos';

        Excel::create($xls_name, function($excel) {

            $excel->sheet('Todos', function($sheet) {
                $contacts = Contact:: all();

                $results = array();

                foreach ($contacts as $contact){

                    $place = Place:: where('id', '=', $contact-> place_id)-> first();

                    $result = array( 'Nombre' => $place-> name,
                                    'País' => $place-> country,
                                    'Localidad' => $place-> locality,
                                    'Zona' => $place-> administrative_area,
                                    'CP' => $place-> postal_code,
                                    'Teléfono' => $place-> phone_number,
                                    'Latitude' => $place-> latitude,
                                    'Longitude' => $place-> longitude,
                                    'Website' => $place-> website,
                                    'Email' => $contact-> email);

                    array_push($results, $result);
                }

                $sheet-> fromArray($results);
            });

        })->download('xls');
    }

    public function show(Search $search)
    {
        return view('searches.show', compact('search'));
    }

    public function edit(Search $search)
    {
    }

    public function destroy(Search $search)
    {
    }

    public function searches_json()
    {
        $data = array();

        $searches = Search::all();

        foreach ($searches as $search){
            $temp_data['id'] = $search-> id;
            $temp_data['name'] = $search-> name;
            $temp_data['keywords'] = $search-> keywords;
            $temp_data['status'] = $this-> status_button($search-> status);

            array_push($data, $temp_data);
        }

        return Datatables::of($data)->make(true);
    }

    public function store(Request $request)
    {
        $newSearch_data = $this-> recalculate_FromTos($request);
        $radius_steps = $this-> calculate_radius_steps($newSearch_data);

        Search::create([
            'name'=> $newSearch_data['name'],
            'from_latitude'=> $newSearch_data['from_latitude'],
            'from_longitude'=> $newSearch_data['from_longitude'],
            'to_latitude'=> $newSearch_data['to_latitude'],
            'to_longitude'=> $newSearch_data['to_longitude'],
            'keywords'=> $newSearch_data['keywords'],
            'radius'=> $radius_steps['radius'],
            'step'=> $radius_steps['step'],
        ]);

        return redirect('/home');
    }

    protected function calculate_radius_steps($positions){
        $radius_steps = array();

        $large_average = $this-> calculate_average($positions);

        if ($large_average <= 0.15) {
            $radius_steps['radius'] = Config::get('constants.default_radius');
            $radius_steps['step'] = Config::get('constants.default_step');
        } else {
            $radius_steps['radius'] = round($large_average/Config::get('constants.min_average')*Config::get('constants.default_radius'));
            $radius_steps['step'] = round($large_average/Config::get('constants.min_average')*Config::get('constants.default_step') * 100) / 100;
        }

        if ($radius_steps['radius'] >= 50000) {
            $radius_steps['radius'] = 50000;
            $radius_steps['step'] = 0.75;
        }

        return $radius_steps;
    }

    protected function calculate_average($positions){
        $large_latitude = $positions['to_latitude']-$positions['from_latitude'];
        $large_longitude = $positions['to_longitude']-$positions['from_longitude'];
        $large_average = ($large_latitude+$large_longitude)/2;

        return $large_average;
    }

    protected function recalculate_FromTos(Request $request) {
        $newSearch_data = array();

        $newSearch_data['name'] = request('name');
        $newSearch_data['keywords'] = request('keywords');

        if (request('from_latitude') > request('to_latitude')) {
            $newSearch_data['from_latitude'] = request('to_latitude');
            $newSearch_data['to_latitude'] =  request('from_latitude');
        } else {
            $newSearch_data['from_latitude'] = request('from_latitude');
            $newSearch_data['to_latitude'] =  request('to_latitude');
        }

        if (request('from_longitude') > request('to_longitude')) {
            $newSearch_data['from_longitude'] = request('to_longitude');
            $newSearch_data['to_longitude'] = request('from_longitude');
        } else {
            $newSearch_data['from_longitude'] = request('from_longitude');
            $newSearch_data['to_longitude'] = request('to_longitude');
        }

        return $newSearch_data;
    }

    public function status_button($status){
        switch ($status){
            case 1:
                return '<input id="btnPause" class="pause btn"/>';
                break;
            case 0:
            case 2:
                return '<input id="btnRun" class="run btn"/>';
                break;
            case 3:
                return '<input id="btnFinished" class="finished btn"/>';
                break;
        }
    }

    public function countries(Request $request)
    {
        if($request->country != "all"){
            return view('home', [           
                                'projectsCountry'=> Search::projectsCountry($request->country, Auth::User()->id)->get(),
                            ]);
        }else{
            return redirect('home');
        }
        
    }

    public function index(Request $request)
    {
        if($request->id != null){
            return view('home', [           
                                'searchSelected'=> Search::find($request->id),
                            ]);
        }else{
            return redirect('home');
        }
        
    }
}
