@if(isset($searchSelected))
<section id="mapView" >
    <div id='contMap'>
        <div id='map'>Loading...</div>
    </div>

    <?php
        $mapCenter = "{lat: 0, lng: 0}";
        $zoomMap = "5";
        $res =  "<script>
                        var map;
                        var datapba;
                        var datalbta;
                        var databbta;
                        var heatmap;
                        var markerCluster;
                        var markersMap = new Array();
                        var positions = new Array();
                        var isMarkers = true;
                        var pingreen ='". asset('plugins/google/img/markets/pingreen.png') ."'; 
                        function initMap() {
                            map = new google.maps.Map(document.getElementById('map'), {
                                zoom: ". $zoomMap .",
                                center: ". $mapCenter .",
                                disableDefaultUI: true,
                                streetViewControl: true,
                                scaleControl: true,
                                zoomControl: true,
                                fullscreenControlOptions: {
                                    position: google.maps.ControlPosition.BOTTOM_RIGHT
                                },
                                fullscreenControl: true
                            });
                            var infoWindow = new google.maps.InfoWindow;
                            downloadUrl('".route('geojson',$searchSelected) ."', function(data) {
                                var xml = data.responseXML;
                                var markers = xml.documentElement.getElementsByTagName('marker');
                                Array.prototype.forEach.call(markers, function(markerElem) {
                                    var name = markerElem.getAttribute('Name');
                                    var address = markerElem.getAttribute('Phones');
                                    var type = markerElem.getAttribute('Website');
                                    var point = new google.maps.LatLng(
                                        parseFloat(markerElem.getAttribute('latitude')),
                                        parseFloat(markerElem.getAttribute('longitude'))
                                    );
                                    positions.push(point);
                                    var infowincontent = document.createElement('div');
                                    var strong = document.createElement('strong');
                                    strong.textContent = name
                                    infowincontent.appendChild(strong);
                                    infowincontent.appendChild(document.createElement('br'));

                                    var text = document.createElement('text');
                                    text.textContent = address
                                    infowincontent.appendChild(text);
                                    var marker = new google.maps.Marker({
                                        map: map,
                                        position: point,
                                        icon: pingreen                                  
                                    });
                                    markersMap.push(marker);
                                    marker.addListener('click', function() {
                                        infoWindow.setContent(infowincontent);
                                        infoWindow.open(map, marker);
                                    });
                                    map.setCenter(point);
                                });
                                markerCluster = new MarkerClusterer(map, markersMap,{imagePath: '". asset('plugins/google/img/markets/m') ."'});
                                heatmap = new google.maps.visualization.HeatmapLayer({
                                        data: positions
                                });
                                heatmap.set('radius',(positions.length>7000)?150:15);
                            });
                        }
                        function loadSatellite(){
                            map.setMapTypeId('hybrid');
                        }
                        function loadRoad(){
                            map.setMapTypeId('roadmap');
                        }
                        function loadHeatMap(){
                            heatmap.setMap(heatmap.getMap() ? null : map); 
                        }  
                        function loadMarkers(){
                            if(isMarkers){
                                markerCluster.clearMarkers();
                                isMarkers= false;
                            }else{
                                markerCluster.clearMarkers();
                                markerCluster.addMarkers(markersMap);
                                markerCluster.redraw();
                                isMarkers= true;
                            }
                        }
                        function loadLayerpba(){
                            if(datapba==null){
                                datapba = new google.maps.Data();
                                datapba.loadGeoJson('".asset('resources/json/argentina/pba.json')."');
                                datapba.setStyle(function(feature) {
                                    var population = feature.getProperty('TOT_POB');
                                    var color=population>150?population>300?population>600?population>1000?population>2000?population>3000?0.8:0.5:0.3:0.2:0.1:0.03:0;
                                    return {
                                        fillColor: 'red',
                                        fillOpacity: color,
                                        strokeWeight:0
                                    };
                                });
                                imageURL = '".asset('plugins/mine/img/png/poblacion.png')."';
                                datapba.addListener('click', function(event) {
                                    swal('INFORMACIÓN POBLACIONAL', 
                                    'Hogares:  '+event.feature.getProperty('HOGARES')+'\\n'+
                                    'Varones:  '+event.feature.getProperty('VARONES')+'\\n'+
                                    'Mujeres:  '+event.feature.getProperty('MUJERES')+'\\n'+
                                    'Población total:  '+event.feature.getProperty('TOT_POB'), 
                                    imageURL);
                                });
                                map.data.setMap(null);
                                datapba.setMap(map);
                            }else{
                                datapba.setMap(null);
                                datapba = null;
                            }
                        }
                        function loadLayerlbta(){
                            if(datalbta==null){
                                datalbta = new google.maps.Data();
                                datalbta.loadGeoJson('".asset('resources/json/colombia/lbta.geojson')."');
                                datalbta.setStyle(function(feature) {
                                    return {
                                        fillColor: 'red',
                                        fillOpacity: 0.1,
                                        strokeWeight:1,
                                        strokeColor: '#aa0004'
                                    };
                                });
                                datalbta.addListener('click', function(event) {
                                    swal(event.feature.getProperty('NOMBRE'), 
                                    'Localidad de Bogotá', 
                                    'info');
                                });
                                map.data.setMap(null);
                                datalbta.setMap(map);
                            }else{
                                datalbta.setMap(null);
                                datalbta = null;
                            }
                        }
                        function loadLayerbbta(){
                            if(databbta==null){
                                databbta = new google.maps.Data();
                                databbta.loadGeoJson('".asset('resources/json/colombia/bbta.geojson')."');
                                databbta.setStyle(function(feature) {
                                    return {
                                        fillColor: 'blue',
                                        fillOpacity: 0.1,
                                        strokeWeight:1,
                                        strokeColor: 'blue'
                                    };
                                });
                                databbta.addListener('click', function(event) {
                                   swal(event.feature.getProperty('name'), 
                                    'Barrio de Bogotá', 
                                    'info');
                                });
                                map.data.setMap(null);
                                databbta.setMap(map);
                            }else{
                                databbta.setMap(null);
                                databbta = null;
                            }
                        }
                        function downloadUrl(url, callback) {
                            var request = window.ActiveXObject ?
                                new ActiveXObject('Microsoft.XMLHTTP') :
                                new XMLHttpRequest;
                            request.onreadystatechange = function() {
                                if (request.readyState == 4) {
                                    request.onreadystatechange = doNothing;
                                    callback(request, request.status);
                                }
                            };
                            request.open('GET', url, true);
                            request.send(null);
                        }
                        function doNothing() {}
                    </script>
                    "; 
            echo $res;
    ?>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHz1WQph6afacfHgrIE1PjrnvmgGQTzeA&callback=initMap&libraries=visualization">
    </script>
    <script src="{{asset('plugins/mine/js/sweetalert.min.js')}}"></script>
</section>
@else
<section id="projectsView" class="container-fluid m-3 section" >
    @if(isset($projectsCountry))
        <h1 class="mb-4">{{$projectsCountry[0]->country}}</h1>
        <div id="projectsContainer">
            @foreach($projectsCountry as $search)
                <div class="projectCard" onclick="location.href = '{{ route('search.index',['id'=> $search->id] ) }}'">
                    <heading><img src="{{ asset('storage/imgProjects/'. Auth::User()->id.$search->id .'.jpg') }}"></heading>
                    <article><p class="m-1">{{$search->name}}</p></article>
                </div>
            @endforeach
        </div>
    @else
        <div id="projectsContainer">
            @foreach(Auth::User()->searches as $search)
                <div class="projectCard" onclick="location.href = '{{ route('search.index',['id'=> $search->id] ) }}'">
                    <heading><img src="{{ asset('storage/imgProjects/'. Auth::User()->id.$search->id .'.jpg') }}"></heading>
                    <article><p class="m-1">{{$search->name}}</p></article>
                </div>
            @endforeach
        </div>
    @endif
</section>
@endif



