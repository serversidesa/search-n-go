@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="text-align:right;">
                    <a href="searches/create" class="button">New Search</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="text-align:right;">
                    <a href="searches/resultsByContacts">Download All</a> 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('includes.SearchesTable') 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
