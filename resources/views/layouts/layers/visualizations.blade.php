<div class="container-layers">
    <div class="layerCard" data-dismiss="modal" aria-label="Close" onclick="loadRoad();">
        <heading>Mapa</heading>
        <article><img src="{{ asset('plugins/mine/img/jpg/mapa.jpg') }}"></article>
    </div>
    <div class="layerCard" data-dismiss="modal" aria-label="Close" onclick="loadSatellite();">
        <heading>Satélite</heading>
        <article><img src="{{ asset('plugins/mine/img/jpg/satelital.jpg') }}"></article>
    </div>
</div>    
<div class="container-layers">
    <div class="layerCard" data-dismiss="modal" aria-label="Close" onclick="loadMarkers();">
        <heading>Marcadores</heading>
        <article><img src="{{ asset('plugins/mine/img/jpg/marcadores.jpg') }}"></article>
    </div>
    <div class="layerCard" data-dismiss="modal" aria-label="Close" onclick="loadHeatMap();">
        <heading>Heat-Map</heading>
        <article><img src="{{ asset('plugins/mine/img/jpg/heatmap.jpg') }}"></article>
    </div>
</div>    