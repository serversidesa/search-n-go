<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Config;
use Illuminate\Support\Facades\Log;

use App\Services\PayUService\Exception;
use App\Http\Controllers\PlacesController;
use App\Classes\GooglePlacesClient;
use App\Classes\GooglePlaces;
use App\Place;

class Search extends Model
{
   	protected $guarded = [];

   	public function places() {
		return $this-> hasMany(Place::class);
	}

	public function run() {
        $this-> setStatus('running');

        $this-> scan_zone();
        $this-> scan_places();
        $this-> scan_contacts();

        $this-> setStatus('completed');
    }

    public function pause() {
        $this-> setStatus('paused');
        $this-> ifScanStatus_setScanStatus('running', 'paused');
        $this-> ifPlacesStatus_setPlacesStatus('running', 'paused');
        $this-> ifContactsStatus_setContactsStatus('running', 'paused');

        return redirect('/home');
    }

    protected function scan_zone() {
        switch ($this-> scan_status) {
            case Config::get('constants.status_stopped'):
            case Config::get('constants.status_paused'):

                if ($this-> isScanStopped()){
                    $this-> setFromCoord();
                } else {
                    $this-> actual_latitude    = $this-> actual_latitude;
                    $this-> actual_longitude   = $this-> actual_longitude;
                }

                $this-> setScanStatus('running');

                Log::debug('['.$this-> id.'] Step = '.$this-> step);
                Log::debug('['.$this-> id.'] Radius = '.$this-> radius);

                do {
                    do {
                        if ($this-> isPaused()) {
                            $this-> setScanStatus('paused');
                            return redirect('/home');
                            exit;
                        }

                        foreach ($this-> getKeywords() as $keyword) {
                            Log::debug('['.$this-> id.'] Finding Places for keyword '.$keyword);
                            $this-> find_places($keyword); 
                        }

                        $this-> grow_latitude(); 

                    } while ($this-> actual_latitude <= $this-> to_latitude);

                    $this-> grow_longitude();

                } while ($this-> actual_longitude <= $this-> to_longitude);

                $this-> setScanStatus('completed');

                return redirect('/home');
                break;
        }
    }

    protected function grow_latitude() {
        $this-> actual_latitude += $this-> step;
        $this-> save();

        Log::debug('['.$this-> id.'] Growing Latitude to '.$this-> actual_latitude);
        return $this-> actual_latitude;
    }

    protected function grow_longitude() {
        $this-> actual_longitude += $this-> step;

        if ($this-> even == 1) {
            $this-> actual_latitude = $this-> from_latitude-($this-> step / 2);
            $this-> even = 0;
        } else{
            $this-> actual_latitude = $this-> from_latitude;
            $this-> even = 1;
        }
        $this-> save();

        Log::debug('['.$this-> id.'] Growing Longitude to '.$this-> actual_longitude);
        Log::debug('['.$this-> id.'] Growing Latitude to '.$this-> actual_latitude);
        return $this-> actual_longitude;
    }

    protected function scan_places() {
        switch ($this-> places_status) {
            case Config::get('constants.status_stopped'):
            case Config::get('constants.status_paused'):
            	$this-> setPlacesStatus('running');

                foreach ($this-> getPlacesByStatus('stopped') as $place) {
                    if ($this-> isPaused()) {
                        $this-> setPlacesStatus('paused');

                        return redirect('/home');
                        exit;
                    }

                    $place-> find_details();
                }

                $this-> setPlacesStatus('completed');
                break;
        }
    }

    protected function scan_contacts() {
        switch ($this-> contacts_status) {
            case Config::get('constants.status_stopped'):
            case Config::get('constants.status_paused'):

                $this-> setContactsStatus('running');

                foreach ($this-> getPlacesByContactsStatus('stopped') as $place) {
                    if ($this-> isPaused()) {
                        $this-> setContactsStatus('paused');

                        return redirect('/home');
                        exit;
                    }

                    $place-> find_contacts();
                }

                $this-> setContactsStatus('completed');
                break;
        }
    }

    protected function find_places($keyword) {
        try {

            $google_places = new GooglePlaces(Config::get('constants.apiKey'));
            $google_places->location = array($this-> actual_latitude, $this-> actual_longitude);
            $google_places->radius   = $this-> radius;
            $google_places->keyword  = $keyword;
            $google_places->name     = $keyword;
            $places_founded          = $google_places->radarSearch();

            if ($places_founded["status"] == 'OK') {

                log::debug('['.$this-> id.'] '.count($places_founded['results']).' places founded');
                
    			foreach ($places_founded['results'] as $place){
                    $exists = Place::where('place_id', '=', $place['place_id'])-> first();
                    if ($exists == null) {
    				    $newPlace = new Place;
                        
    				    $newPlace-> place_id = $place['place_id'];
    				    $newPlace-> item = $keyword;
    				    $newPlace-> search_id = $this-> id;
    				    $newPlace-> status = Config::get('constants.status_stopped');
                        $newPlace-> contacts_status = Config::get('constants.status_stopped');
                        $newPlace-> cuit_status = Config::get('constants.status_stopped');
                        $newPlace-> linkedin_status = Config::get('constants.status_stopped');

        				$newPlace-> save();
                    }
    			}
            }

            unset($google_places);
        } catch (\Exception $e) {
            log::debug($e);
        }
    }

    protected function isPaused(){
        if (Search::find($this-> id)-> status == Config::get('constants.status_paused')){
            return true;
        } else{
            return false;
        }
    }

    protected function isKeywordChange(){
        if ($this-> scan_status == Config::get('constants.status_keyword_change')){
            return true;
        } else{
            return false;
        }
    }

    protected function getKeywords(){
        return explode(',' , $this-> keywords);
    }

    protected function isScanStopped(){
        if ($this-> scan_status == Config::get('constants.status_stopped')){
            return true;
        } else{
            return false;
        }
    }

    protected function setStatus($status){
        $this-> status = Config::get('constants.status_'.$status);
        $this-> save();

        Log::debug('['.$this-> id.'] Search '.$status);
    }

    protected function setScanStatus($status){
        $this-> scan_status = Config::get('constants.status_'.$status);
        $this-> save();

        Log::debug('['.$this-> id.'] ZoneScan '.$status);
    }

    protected function setPlacesStatus($status){
        $this-> places_status = Config::get('constants.status_'.$status);
        $this-> save();

        Log::debug('['.$this-> id.'] PlacesScan '.$status);
    }

    protected function setContactsStatus($status){
        $this-> contacts_status = Config::get('constants.status_'.$status);
        $this-> save();

        Log::debug('['.$this-> id.'] ContactsScan '.$status);
    }

    protected function setFromCoord(){
        $this-> actual_latitude    = $this-> from_latitude;
        $this-> actual_longitude   = $this-> from_longitude;

        $this-> save();

        Log::debug('['.$this-> id.'] Actual Latitude '.$this-> actual_latitude);
        Log::debug('['.$this-> id.'] Actual Longitude '.$this-> actual_longitude);
    }

    protected function getPlacesByStatus($status){
        return $this-> places ->where('status', '=', Config::get('constants.status_'.$status));
    }

    protected function getPlacesByContactsStatus($status){
        return $this-> places ->where('contacts_status', '=', Config::get('constants.status_'.$status));
    }

    protected function getPlacesByNoStatus($statuses){
        $conditions = array();
        foreach ($statuses as $status){
            array_push($conditions, Config::get('constants.status_'.$status));
        }
        return $this-> places ->whereNotIn('status', $conditions);
    }

    protected function ifScanStatus_setScanStatus($prev_status, $to_status){
        if ($this-> scan_status == Config::get('constants.status_'.$prev_status)){
            $this-> scan_status = Config::get('constants.status_'.$to_status);
            $this-> save();

            Log::debug('['.$this-> id.'] Zone Scan '.$to_status);
        }
    }

    protected function ifPlacesStatus_setPlacesStatus($prev_status, $to_status){
        if ($this-> places_status == Config::get('constants.status_'.$prev_status)){
            $this-> places_status = Config::get('constants.status_'.$to_status);
            $this-> save();

            Log::debug('['.$this-> id.'] Places Scan '.$to_status);
        }
    }

    protected function ifContactsStatus_setContactsStatus($prev_status, $to_status){
        if ($this-> contacts_status == Config::get('constants.status_'.$prev_status)){
            $this-> contacts_status = Config::get('constants.status_'.$to_status);
            $this-> save();

            Log::debug('['.$this-> id.'] Contacts Scan '.$to_status);
        }
    }

    public function scopeProjectsCountry($query, $country, $userId){
        return $query->join('users','users.id','=','searches.user_id')
                        ->join('places','searches.id','=','places.search_id')
                        ->select('places.country')
                        ->addSelect('searches.id')
                        ->addSelect('searches.name')
                        ->distinct('places.country')
                        ->where('places.country', '=', $country)
                        ->where('users.id', '=', $userId);
    }
    
    public function scopeCountriesLayers($query, $searchId){
        return $query->join('places','searches.id','=','places.search_id')
                        ->select('places.country')
                        ->where('searches.id', '=', $searchId)
                        ->groupBy('places.country');
    }
}

