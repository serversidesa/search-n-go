<!DOCTYPE html>
<html lang="es">
    <head>
        <title>GEOBIDATA</title>
        <link rel="icon" type="image.png" href="">
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/mine/css/style-index.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/superslides/css/superslides.css')}}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1"> 
        
        <meta name="description" content="Geobidata">
        <meta name="keywords" content="Geobidata, Geolocalization,BI,">
        <meta name="author" content="Binetz.com">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        
    </head>    
    
    <body>
        <header>
            <!--
            <h1 style="font-size: 50px;color: white; font-family: sans-serif;">GEOBIDATA</h1>
            -->
            <img id="imgLogo" src="{{ asset('img/logot.png') }}"/>
            <nav id="scr">
                <ul class="nav nav-pills">
                    <li id="#start" class="active textNav">Start</li>
                    <li id="#ourTec" class="textNav">Technology </li>
                    <li id="#aboutUs" class="textNav">About us</li>
                    <li id="#contac" class="textNav">Contact</li>
                    <a style="text-decoration:none;" href="{{ url('/login') }}"><li id="signin"  class="">Login</li></a>
                </ul>
            </nav>
        </header>
            
            <div id="toggle">
                <img src="{{ asset('img/menu2.png') }}">
            </div>
            <nav id="mob">
                <ul class="nav nav-pills">
                    <li id="#start" class="active">Start</li>
                    <li id="#ourTec" class="">Technology</li>
                    <li id="#aboutUs" class="">About us</li>
                    <li id="#contac" class="">Contact</li>
                    <a style="text-decoration:none;" href="{{ url('/login') }}"><li id="signin"  class="">Login</li></a>
                </ul>
            </nav>
        
        <!--  section start.................. -->
        <section id="start">
            <div id="slides" >
                <ul class="slides-container">
                    <!--
                    <li>
                        <img src="{{ asset('img/slider1.jpg') }}" width="1024" height="683" alt="Cinelli">
                        <div class="container">
                            <h1>WELCOME TO<br>GEOBIDATA</h1>
                            <br><h2>Make Things Happen</h2>
                        </div>
                    </li>
                    -->
                    <li>
                        <img src="{{ asset('img/slider2.jpg') }}" alt="Cinelli">
                        <div class="container">
                            <h2>Download list with potential clients in the area</h2>
                        </div>
                    </li>
                    <li>
                        <img src= "{{ asset('img/slider3.jpg') }}" width="1024" height="682" alt="Surly">
                        <div class="container">
                             <h2>Georeferenced Analysis Solutions</h2>
                        </div>
                    </li>
                    <li> 
                        <img src="{{ asset('img/slider4.jpg') }}" width="1024" height="683" alt="Cinelli">
                        <div class="container">
                            <h2>From anywhere, when you need</h2>
                        </div>
                    </li>
                </ul>

                <nav class="slides-navigation">
                    <a href="#" class="next">
                        <img src="{{ asset('img/circle-right.png') }}">
                    </a>
                    <a href="#" class="prev">
                        <img src="{{ asset('img/circle-left.png') }}">
                    </a>
                </nav>
            </div>
            
        </section>
        
        
        <!-- parallax effect.................. -->
        <section id="first-paral">
        </section>
        
        
        <!-- section our technology.................. -->
        <section id="ourTec">
            <div id="content-ourtec">
                <h1>OUR TECHNOLOGY</h1>
                <div class="articles">
                    <article>
                        <heading>01 / <b>FAST</b></heading>
                        <p>
                            With Integral Geolocation Solutions (IGS) you can search, capture and analyze any location with geographically referenced information. 
                        </p>
                    </article>
                    <article>
                        <heading>02 / <b>SECURE</b></heading>
                        <p>
                            State-of-the-art technology to keep your information private and secure.  
                        </p>
                    </article>
                    <article>
                        <heading>03 / <b>EASY</b></heading>
                        <p>
                            IGS helps you answer questions and view information in a way that is quickly understood and easily searchable—on a map. 
                        </p>
                    </article>
                </div>
            </div>
        </section>
        
        
        
        <!--section our about us.................. -->
        <section id="aboutUs">
            <div id="content-aboutUs">
                <h1>ABOUT US</h1>
                <div class="articles">
                    <article>
                        <div class="contImg">
                            <img src="{{ asset('img/story.png') }}">
                        </div>
                        <heading><br>OUR STORY</heading>
                        <p>
                            GEOBIDATA is a company dedicated to the implementation of Integral Geolocation Solutions (IGS).
                            <br><br>
                            » Our main asset resides in the vast experience of the professionals who make up GEOBIDATA, including different fields of study and professional backgrounds, as well as significant participation in academia.     
                            <br><br>
                            » Meet our professionals and revolutionize your decision-making process, by incorporating Integral Geolocation Solutions (IGS) into your organization.

                        </p>
                    </article>
                     <article>
                        <div class="contImg">
                            <img src="{{ asset('img/vision.png') }}">
                        </div>
                        <heading><br>OUR VISION</heading>
                        <p>
                            We aim to be the benchmark for IGS and modern business applications. Collaborating with organizations to define the processes, technologies and tools needed to transform data into information, information into knowledge and knowledge into plans and actions, to help them achieve their strategic goals and feedback their improvement processes.
                        </p>
                    </article>
                     <article>
                        <div class="contImg">
                            <img src="{{ asset('img/tec.png') }}">
                        </div>
                        <heading><br>OUR TECHNOLOGY</heading>
                        <p>
                            Our technological solutions allow clients to make decisions based on facts and information, rather than intuition. This enables continuous innovation within the organization, adding value to the knowledge held by its members.
                            
                        </p>
                    </article>
                </div>
            </div>
        </section>
        
        <!-- parallax effect.................. -->
        <section id="sec-paral">
        </section>
        
        <!--section our about us.................. -->
        <section id="contac">
            <div id="content-contac">
                <form id="formContact"  action="{{ route('send') }}" method="POST">
                    <h1>CONTACT</h1>
                    <input type="text" name="name" placeholder="Name">
                    <input type="text" name="correo" placeholder="Mail">
                    <input type="text" name="telephone number" placeholder="Telephone">
                    <textarea name="message" placeholder="Message"></textarea>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <input type="submit" form="formContact">
                </form>
            </div>
        </section>
        
        
        <div id="ir-arriba" class="up" >
            <img class="up" src="{{ asset('img/circle-up.png') }}">
        </div>
        
        <script src="{{ asset('plugins/jquery/js/jquery-3.2.1.js') }}"></script> 
        <script src="{{ asset('plugins/mine/js/animation.js') }}"></script>
        <script src="{{ asset('plugins/superslides/js/jquery.superslides.js') }}"></script>
        
    </body>
    
    
</html>