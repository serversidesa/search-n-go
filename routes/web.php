<?php

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/searches/resultsByContacts', 'SearchesController@resultsByContacts');

Route::post('/searches', 'SearchesController@store');
Route::get('/searches', 'SearchesController@searches_json');

Route::get('/searches/create', 'SearchesController@create');

Route::get('/searches/{search}', 'SearchesController@show');

Route::get('/searches/{search}/run', 'SearchesController@run');
Route::get('/searches/{search}/pause', 'SearchesController@pause');

Route::get('/searches/{search}/resultsByPlaces', 'SearchesController@resultsByPlaces');

Route::get('/', function () {
    return view('welcometwo');
});

Route::get('/searchngo', 'SearchnGoController@index')->name('home');

Route::resource('search','SearchesController');

Route::get('country/{country}', 'SearchesController@countries')->name('search.country');

Route::get('geojson/{id}', function ($id) {
    $DOMDocument = new DOMDocument("1.0");
    $Raiz = new DOMElement("Markers");
    $nodeAc = $DOMDocument->appendChild($Raiz);
    foreach(App\Search::find($id)->places as $place){
        $newNode = $nodeAc->appendChild(new DOMElement("marker"));
        $newNode->setAttribute("Name",parseToXML($place["name"]));
        $newNode->setAttribute("Phones",parseToXML($place->places["phone_number"].",".$place["international_phone_number"]));
        $newNode->setAttribute("Website",parseToXML($place["website"]));
        $newNode->setAttribute("latitude",parseToXML($place["latitude"]));
        $newNode->setAttribute("longitude",parseToXML($place["longitude"]));
    }
    header("Content-Type: text/xml");    //////////////////////
    echo $DOMDocument->saveXML();
})->middleware('auth')->name('geojson');

function parseToXML($htmlStr){
        $xmlStr=str_replace('<','&lt;',$htmlStr);
        $xmlStr=str_replace('>','&gt;',$xmlStr);
        $xmlStr=str_replace('"','&quot;',$xmlStr);
        $xmlStr=str_replace("'",'&#39;',$xmlStr);
        $xmlStr=str_replace("&",'&amp;',$xmlStr);
        return utf8_encode($xmlStr);
}

Route::post('send',function(Request $request){  
    Mail::send(
        'emails.info', 
        ['name' => $request->name], 
        function(Message $message){
            $message->to('To@example.com','a')
                    ->from('From@example.com', 'a')
                    ->subject('Informacion de contacto');
        }
    );
    return view('welcometwo');
})->name('send');