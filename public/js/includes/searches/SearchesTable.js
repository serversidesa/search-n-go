$(document).ready(function (){
    $('#SearchesTable').DataTable({
        responsive: true,

        "ajax": {
            url: '/searches',
            type: 'GET',
        },

        "columns": [
        { "data": "id" },
        { "data": "name" },
        { "data": "keywords" },
        { "data": "status" },
        ],

        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
            },
            { 
                "targets": [ 3 ],
                "render": function ( data, type, row ) {
                    return $('<div/>').html(row['status']).text();
                }
            }
        ],

        "fnInitComplete": function(oSettings) {
            $('.dataTables_scrollHead thead').hide();
        }
    });

    $('#SearchesTable tbody').on( 'click', '.run', function () {
        var row = $('#SearchesTable').DataTable().row( $(this).parents('tr') ).data();

        $.ajax({
            type: "GET",
            url: '/searches/'+row['id']+'/run',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        });

        location.reload();
    });

    $('#SearchesTable tbody').on( 'click', '.pause', function () {
        var row = $('#SearchesTable').DataTable().row( $(this).parents('tr') ).data();

        $.ajax({
            type: "GET",
            url: '/searches/'+row['id']+'/pause',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        });

        location.reload();
    });

    $('#SearchesTable tbody').on( 'click', '.finished', function () {
        var row = $('#SearchesTable').DataTable().row( $(this).parents('tr') ).data();

        window.location.href = '/searches/'+row['id']+'/resultsByPlaces';
    });
});