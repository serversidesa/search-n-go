<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
    <h2>Searches</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li style="float:right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="SearchesTable" name="SearchesTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>id</th>
            <th>Name</th>
            <th>Keywords</th>
            <th>Status</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>id</th>
            <th>Name</th>
            <th>Keywords</th>
            <th>Status</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>