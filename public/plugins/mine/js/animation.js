$(document).ready(function () { 
    changeSize();
    $('nav ul li').click(function () {
        $('nav ul li').removeClass('active');
        $(this).addClass('active');
        
    });
    
    $('nav ul li').click(function () {
        var addressValue = $(this).attr("id");
        var dest = $(addressValue).offset().top;
        $('html, body').animate({scrollTop: (dest)}, 1000);
    });
    
    $('.up').click(function() {
		$('body, html').animate({
			scrollTop: '0px'
		}, 1000);
	});
 
	$(window).scroll(function() {
		if ($(this).scrollTop() > 0) {
			$('#ir-arriba').slideDown(300);
		} else {
			$('#ir-arriba').slideUp(300);
		}
	});
    
    $('#toggle').click(function() {
        $('#mob').slideToggle(400);
    });
    
    $('nav#mob ul li').click(function() {
        $('#mob').slideToggle(400);
    });
    
    $(window).resize(function() {
        if($('#mob').is(':visible')){
            $('#mob').hide();
        }
        changeSize();
    });
    
    $('#button').click(function(){
        $('#formContact')[0].reset();
        alert("We appreciate you contacting us");
    });
});

function changeSize(){
    var h = $(window).height();
    $('#start').height(h);
}

