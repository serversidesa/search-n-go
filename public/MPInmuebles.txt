<!DOCTYPE html>
<html lang="ES">
<head>
<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
<meta name="title" content="Inmobiliaria en alquiler y venta  en Capital Federal | MP Inmuebles - Servicios Inmobiliarios"/>
<meta name="description" content="MP Inmuebles - Servicios Inmobiliarios | Propiedades - Compra - Venta - Alquiler de propiedades"/>
<meta name="keywords" content="inmobiliarias,inmobiliaria,departamentos,casas,chacras,campos,locales,terrenos,emprendimientos,capital federal"/>
<meta property="og:image" content="http://mpinmuebles.com.ar/img/logo_pub.png"/>
<meta property="og:image:url" content="http://mpinmuebles.com.ar/img/logo_pub.png"/>
<meta property="og:image:secure_url" content="http://mpinmuebles.com.ar/img/logo_pub.png"/>
<title>Inmobiliaria en alquiler y venta  en Capital Federal - MP Inmuebles - Servicios Inmobiliarios</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/linearicons.css"/>
<link rel="stylesheet" type="text/css" href="css/index.css"/>
<!--[if lt IE 9]>
<script src="js/html5shiv.min.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
</head>
<body>
<header class="header-default">
	<!--
	<div class="top-bar">
		<div class="container">
			<div class="top-bar-left left">
				<ul class="top-bar-item right social-icons">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="top-bar-right right">
				<a href="login.html" class="top-bar-item"><i class="fa fa-sign-in icon"></i>Login</a>
				<a href="register.html" class="top-bar-item"><i class="fa fa-user-plus icon"></i>Register</a>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	-->
	<div class="container">
		<div class="navbar-header">
			<div class="header-details">
				<div class="header-item header-phone left">
					<table>
						<tr>
							<td><i class="lnr lnr-map-marker"></i></td>
							<td class="header-item-text">¿Dónde estamos?<br/><span>Av. Emilio Castro 7075<em>CABA (1408)</em></span></td>
						</tr>
					</table>
				</div>
				<div class="header-item header-phone left">
					<table>
						<tr>
							<td><i class="lnr lnr-phone-handset"></i></td>
							<td class="header-item-text">Llamános al<br/><span>4641 3826</span></td>
						</tr>
					</table>
				</div>
				<div class="header-item header-phone left">
					<table>
						<tr>
							<td><i class="lnr lnr-envelope"></i></td>
							<td class="header-item-text">Escribinos a<br/><span><a href="mailto:ventas@mpinmuebles.com.ar">ventas@mpinmuebles.com.ar</a></span></td>
						</tr>
					</table>
				</div>
				<div class="clear"></div>
			</div>
			<a class="navbar-brand" href="index"><img src="img/logo.png" alt="MP Inmuebles - Servicios Inmobiliarios"/></a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    		<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
  </button>
		</div>
<style type="text/css">
.button-facebook .fa,.button-facebook{background:#2d55a5!important}
.button-facebook:hover,.button-facebook:hover .fa{background:#416ecd!important}
@media all and (max-width: 1200px){
.header-default .nav.navbar-nav li a{padding:20px 10px}
.header-default .member-actions .button{font-size:13px;padding:22px 15px 19px 30px}
}
</style>
		<div class="navbar-collapse collapse">
			<div class="main-menu-wrap">
				<div class="container-fixed">
                    <div class="member-actions right">
                        <a href="https://www.facebook.com/MasPosibilidades/" class="button small alt button-icon button-facebook" target="_blank"><i class="fa fa-facebook"></i> Seguínos en Facebook</a>
                        <a href="contacto" class="button small alt button-icon"><i class="lnr lnr-bubble"></i> Contacto</a>
                    </div>
					<ul class="nav navbar-nav right">
						<li class="current-menu-item"><a href="index">Inicio</a></li>
						<li><a href="nosotros">Nosotros</a></li>
						<li><a href="propiedades?ope[]=V&p=0&tipo[]=All&ord=preciomenor&in_iub=">Ventas</a></li>
						<li><a href="propiedades?ope[]=A&p=0&tipo[]=All&ord=preciomenor&in_iub=">Alquileres</a></li>
						<li><a href="emprendimientos">Emprendimientos</a></li>
						<li><a href="tasaciones">Tasaciones</a></li>
					</ul>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</header><section class="subheader simple-search">
	<div class="container">
		<h1>Encontrá el lugar que <strong>siempre soñaste...</strong></h1>
		<p class="subtit">Ventas, <strong>alquileres</strong>, tasaciones y mucho más.</p>
		<form class="simple-search-form" action="propiedades" method="GET">
			<input type="hidden" name="ope[]" value="V" id="ope"/>
			
        <input type="hidden" name="p" value="0"/>
        <input type="hidden" name="ord" value="preciomayor"/>
        			<div class="tabs">
				<ul>
					<li><a href="#tabs-1" id="busven">Venta</a></li>
					<li><a href="#tabs-1" id="busalq">Alquiler</a></li>
					<li><a href="#tabs-1" id="codfic">Código</a></li>
				</ul>
				<div id="tabs-1" class="ui-tabs-hide">
					<input type="hidden" name="in_iub" id="in_iub"/>
					<input type="text" size="25" id="campobusq" onkeyup="suggest(this.value);" onblur="fill();" class="area" placeholder="Ingresa la localidad o barrio..." autocomplete="off"/>
					<div class="suggestionsBox" id="suggestions" style="display:none">
						<div class="suggestionList" id="suggestionsList">&nbsp;</div>
					</div>
					<input type="submit" value="Buscar"/>
				</div>
			</div>
		</form>
	</div>
</section>
<section class="module services">
	<div class="container">
		<div class="row">
			<a class="col-lg-4 col-md-4" href="propiedades?ope=V&p=0&tipo=All&ord=preciomenor">
				<div class="service-item shadow-hover">
					<i class="fa lnr lnr-home"></i>
					<h4>Ventas</h4>
					<p>Tenemos todas las posibilidades para que concretes tus sueños</p>
				</div>
			</a>
			<a class="col-lg-4 col-md-4" href="propiedades?ope=A&p=0&tipo=All&ord=preciomenor">
				<div class="service-item shadow-hover">
					<i class="fa lnr lnr-apartment"></i>
					<h4>Alquileres</h4>
					<p>Consulta por nuestras propiedades con o sin garantía propietaria</p>
				</div>
			</a>
			<a class="col-lg-4 col-md-4" href="emprendimientos">
				<div class="service-item shadow-hover">
					<i class="fa lnr lnr-construction"></i>
					<h4>Emprendimientos</h4>
					<p>La mejor manera de predecir el futuro es construyéndolo. Contamos con una opción para cada necesidad.</p>
				</div>
			</a>
		</div>
	</div>
</section>
<section class="module no-padding properties featured">
	<div class="container">
		<div class="module-header">
			<h2>Tenemos la propiedad <strong>que buscás</strong></h2>
			<img src="img/divider.png"/>
			<p>Tu inmueble de la manera más fácil y rápida</p>
		</div>
	</div>
	<div class="slider-nav slider-nav-properties-featured">
		<span class="slider-prev"><i class="fa fa-angle-left"></i></span>
		<span class="slider-next"><i class="fa fa-angle-right"></i></span>
	</div>
	<div class="slider-wrap">
		<div class="slider slider-featured">
			<div class="property property-hidden-content slide">
				<a href="departamento-en-venta-en-villa-luro-ficha-mpi1389" class="property-content">
					<div class="property-title">
						<h4>Departamento en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Rivadavia, Av. al 9500</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 2 AMBIENTES</td>
														<td><i class="fa fa-tint"></i> 1 baño</td>
														<td><i class="lnr lnr-move"></i> 49.00m²</td>
													</tr>
					</table>
				</a>
				<a href="departamento-en-venta-en-villa-luro-ficha-mpi1389" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Villa Luro</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>1389</strong></div>
					<div class="property-price">U$S 120000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/mpi1389_2.jpg" alt="Departamento en  Villa Luro 2  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="departamento-en-venta-en-liniers-ficha-mpi1234" class="property-content">
					<div class="property-title">
						<h4>Departamento en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Fonrouge al 500</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 3 AMBIENTES</td>
														<td><i class="fa fa-tint"></i> 1 baño</td>
														<td><i class="lnr lnr-move"></i> 65.38m²</td>
													</tr>
					</table>
				</a>
				<a href="departamento-en-venta-en-liniers-ficha-mpi1234" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Liniers</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>1234</strong></div>
					<div class="property-price">U$S 140000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/MPI1234_2.jpg" alt="Departamento en  Liniers 3  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="departamento-en-venta-en-liniers-ficha-mpi1235" class="property-content">
					<div class="property-title">
						<h4>Departamento en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Fonrouge al 500</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 3 AMBIENTES</td>
														<td><i class="fa fa-tint"></i> 2 baño</td>
														<td><i class="lnr lnr-move"></i> 81.19m²</td>
													</tr>
					</table>
				</a>
				<a href="departamento-en-venta-en-liniers-ficha-mpi1235" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Liniers</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>1235</strong></div>
					<div class="property-price">U$S 140000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/MPI1235_2.jpg" alt="Departamento en  Liniers 3  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="departamento-en-venta-en-liniers-ficha-mpi1247" class="property-content">
					<div class="property-title">
						<h4>Departamento en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Ibarrola al 6400</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 2 AMBIENTES</td>
														<td><i class="fa fa-tint"></i> 1 baño</td>
														<td><i class="lnr lnr-move"></i> 52.36m²</td>
													</tr>
					</table>
				</a>
				<a href="departamento-en-venta-en-liniers-ficha-mpi1247" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Liniers</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>1247</strong></div>
					<div class="property-price">U$S 120000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/MPI1247_2.jpg" alt="Departamento en  Liniers 2  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="departamento-en-venta-en-almagro-ficha-mpi821" class="property-content">
					<div class="property-title">
						<h4>Departamento en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Marmol, Jose al 600</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 2 AMBIENTES</td>
														<td><i class="fa fa-tint"></i>  baño</td>
														<td><i class="lnr lnr-move"></i> 67.00m²</td>
													</tr>
					</table>
				</a>
				<a href="departamento-en-venta-en-almagro-ficha-mpi821" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Almagro</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>821</strong></div>
					<div class="property-price">U$S 157000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/MPI821_2.jpg" alt="Departamento en  Almagro 2  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="departamento-en-venta-en-almagro-ficha-mpi822" class="property-content">
					<div class="property-title">
						<h4>Departamento en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Marmol, Jose al 600</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 2 AMBIENTES</td>
														<td><i class="fa fa-tint"></i>  baño</td>
														<td><i class="lnr lnr-move"></i> 56.00m²</td>
													</tr>
					</table>
				</a>
				<a href="departamento-en-venta-en-almagro-ficha-mpi822" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Almagro</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>822</strong></div>
					<div class="property-price">U$S 152000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/MPI822_2.jpg" alt="Departamento en  Almagro 2  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="departamento-en-venta-en-almagro-ficha-mpi828" class="property-content">
					<div class="property-title">
						<h4>Departamento en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Marmol, Jose al 600</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> MONOAMBIENTE</td>
														<td><i class="fa fa-tint"></i>  baño</td>
														<td><i class="lnr lnr-move"></i> 50.00m²</td>
													</tr>
					</table>
				</a>
				<a href="departamento-en-venta-en-almagro-ficha-mpi828" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Almagro</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>828</strong></div>
					<div class="property-price">U$S 130000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/MPI828_2.jpg" alt="Departamento en  Almagro monoambiente " onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="casa-en-venta-en-mataderos-ficha-mpi1485" class="property-content">
					<div class="property-title">
						<h4>Casa en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Montiel al 1900</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 4 AMBIENTES</td>
														<td><i class="fa fa-tint"></i> 3 baño</td>
														<td><i class="lnr lnr-move"></i> 282.00m²</td>
													</tr>
					</table>
				</a>
				<a href="casa-en-venta-en-mataderos-ficha-mpi1485" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Mataderos</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>1485</strong></div>
					<div class="property-price">U$S 480000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/mpi1485_2.jpg" alt="Casa en  Mataderos 4  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="casa-en-venta-en-floresta-ficha-mpi1422" class="property-content">
					<div class="property-title">
						<h4>Casa en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Martinez Castro al 400</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 6 AMBIENTES</td>
														<td><i class="fa fa-tint"></i>  baño</td>
														<td><i class="lnr lnr-move"></i> 243.00m²</td>
													</tr>
					</table>
				</a>
				<a href="casa-en-venta-en-floresta-ficha-mpi1422" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Floresta</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>1422</strong></div>
					<div class="property-price">U$S 280000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/MPI1422_2.jpg" alt="Casa en  Floresta 6  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="casa-en-venta-en-flores-ficha-mpi1421" class="property-content">
					<div class="property-title">
						<h4>Casa en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Lautaro al 900</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 6 AMBIENTES</td>
														<td><i class="fa fa-tint"></i> 3 baño</td>
														<td><i class="lnr lnr-move"></i> 395.00m²</td>
													</tr>
					</table>
				</a>
				<a href="casa-en-venta-en-flores-ficha-mpi1421" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Flores</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>1421</strong></div>
					<div class="property-price">Consultar</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/MPI1421_2.jpg" alt="Casa en  Flores 6  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="departamento-en-venta-en-almagro-ficha-mpi831" class="property-content">
					<div class="property-title">
						<h4>Departamento en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Marmol, Jose al 600</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 3 AMBIENTES</td>
														<td><i class="fa fa-tint"></i>  baño</td>
														<td><i class="lnr lnr-move"></i> 99.80m²</td>
													</tr>
					</table>
				</a>
				<a href="departamento-en-venta-en-almagro-ficha-mpi831" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Almagro</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>831</strong></div>
					<div class="property-price">U$S 340000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/MPI831_2.jpg" alt="Departamento en  Almagro 3  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
			<div class="property property-hidden-content slide">
				<a href="departamento-en-venta-en-almagro-ficha-mpi832" class="property-content">
					<div class="property-title">
						<h4>Departamento en venta</h4>
						<p class="property-address"><i class="fa fa-map-marker icon"></i> Marmol, Jose al 600</p>
					</div>
					<table class="property-details">
						<tr>
														<td><i class="fa fa-bed"></i> 3 AMBIENTES</td>
														<td><i class="fa fa-tint"></i>  baño</td>
														<td><i class="lnr lnr-move"></i> 99.80m²</td>
													</tr>
					</table>
				</a>
				<a href="departamento-en-venta-en-almagro-ficha-mpi832" class="property-img">
					<div class="img-fade"></div>
										<div class="property-tag button alt featured"><i class="lnr lnr-map-marker"></i> <strong>Almagro</strong>, Capital Federal</div>
					<div class="property-tag button status">MPI<strong>832</strong></div>
					<div class="property-price">U$S 340000</div>
					<div class="property-color-bar"></div>
					<img src="https://xintelweb.com/upload/MPI832_2.jpg" alt="Departamento en  Almagro 3  ambientes" onerror="this.src='img/sin_foto.jpg'" style="width:100%;height:275px;object-fit:cover"/>
				</a>
			</div>
		</div>
	</div>
</section><section class="module testimonials">
	<div class="container">
		<div class="module-header">
			<h2>Tu gran oportunidad <strong>está acá</strong></h2>
			<img src="img/divider.png" alt="Divisor - MP Inmuebles - Servicios Inmobiliarios"/>
			<p>Para quienes ven la vida desde otro ángulo</p>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8">
				<a href="edificio-tonelero-5940-en-liniers-capital-federal-emprendimiento-23" class="property-cat" style="background-image:url(https://xintelweb.com/upload/emprendimientos/MPI23_imgprinc.jpg),url(img/sin_foto.jpg);background-size:cover">
					<h3>Tonelero 5940</h3>
					<div class="color-bar"></div>
					<span class="button small">Posesión <strong>en fecha</strong></span>
				</a>
			</div>
			<div class="col-lg-4 col-md-4">
				<a href="edificio-ibarrola-6465-en-liniers-capital-federal-emprendimiento-25" class="property-cat" style="background-image:url(https://xintelweb.com/upload/emprendimientos/MPI25_imgprinc.jpg),url(img/sin_foto.jpg);background-size:cover">
					<h3>Ibarrola 6465</h3>
					<div class="color-bar"></div>
					<span class="button small">Posesión <strong>en fecha</strong></span>
				</a>
			</div>
			<div class="col-lg-4 col-md-4">
				<a href="edificio-jose-marmol-635-mopou-building-iv-en-almagro-capital-federal-emprendimiento-3" class="property-cat" style="background-image:url(https://xintelweb.com/upload/emprendimientos/MPI3_imgprinc.jpg),url(img/sin_foto.jpg);background-size:cover">
					<h3>Jose Marmol 635 - Mopou Building Iv</h3>
					<div class="color-bar"></div>
					<span class="button small">Posesión <strong>en fecha</strong></span>
				</a>
			</div>
			<div class="col-lg-4 col-md-4">
				<a href="edificio-caaguazu-7356-en-liniers-capital-federal-emprendimiento-35" class="property-cat" style="background-image:url(https://xintelweb.com/upload/emprendimientos/MPI35_imgprinc.jpg),url(img/sin_foto.jpg);background-size:cover">
					<h3>Caaguazu 7356</h3>
					<div class="color-bar"></div>
					<span class="button small">Posesión <strong>en fecha</strong></span>
				</a>
			</div>
			<div class="col-lg-4 col-md-4">
				<a href="edificio-montiel-1138-en-liniers-capital-federal-emprendimiento-19" class="property-cat" style="background-image:url(https://xintelweb.com/upload/emprendimientos/MPI19_imgprinc.jpg),url(img/sin_foto.jpg);background-size:cover">
					<h3>Montiel 1138</h3>
					<div class="color-bar"></div>
					<span class="button small">Posesión <strong>en fecha</strong></span>
				</a>
			</div>
			<div class="col-lg-4 col-md-4">
				<a href="edificio-cossio-6275-en-liniers-capital-federal-emprendimiento-32" class="property-cat" style="background-image:url(https://xintelweb.com/upload/emprendimientos/MPI32_imgprinc.jpg),url(img/sin_foto.jpg);background-size:cover">
					<h3>Cossio 6275</h3>
					<div class="color-bar"></div>
					<span class="button small">Posesión <strong>en fecha</strong></span>
				</a>
			</div>
			<div class="col-lg-4 col-md-4">
				<a href="edificio-pizarro-5551-en-villa-luro-capital-federal-emprendimiento-36" class="property-cat" style="background-image:url(https://xintelweb.com/upload/emprendimientos/MPI36_imgprinc.jpg),url(img/sin_foto.jpg);background-size:cover">
					<h3>Pizarro 5551</h3>
					<div class="color-bar"></div>
					<span class="button small">Posesión <strong>en fecha</strong></span>
				</a>
			</div>
			<div class="col-lg-4 col-md-4">
				<a href="edificio-murguiondo-1030-en-liniers-capital-federal-emprendimiento-37" class="property-cat" style="background-image:url(https://xintelweb.com/upload/emprendimientos/MPI37_imgprinc.jpg),url(img/sin_foto.jpg);background-size:cover">
					<h3>Murguiondo 1030</h3>
					<div class="color-bar"></div>
					<span class="button small">Posesión <strong>en fecha</strong></span>
				</a>
			</div>
		</div>
	</div>
</section><section class="module cta newsletter">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-7">
				<h3>Suscribíte a nuestro <strong>newsletter</strong></h3>
				<p>Conocé algunas de todas las opciones que tenemos para vos en <strong>MP Inmuebles</strong></p>
			</div>
			<div class="col-lg-5 col-md-5">
				<form method="post" id="newsletter-form" class="newsletter-form" action="envio">
					<input type="hidden" name="formulario" value="newsletter"/>
					<input type="email" placeholder="Tu e-mail..."/>
					<button type="submit" form="newsletter-form"><i class="fa fa-send"></i></button>
				</form>
			</div>
		</div>
	</div>
</section>
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-4 widget footer-widget">
				<a class="footer-logo" href="index"><img src="img/logo.png" alt="MP Inmuebles - Servicios Inmobiliarios"/></a>
				<p>Somos una empresa joven con suficiente experiencia en el mercado inmobiliario. Ponemos toda la tecnología mas moderna al servicio de nuestros clientes publicando en todos los medios hasta vender. Con nosotros, ¡tenes Mas Posibilidades de Vender!</p>
				<div class="divider"></div>
				<ul class="social-icons circle">
					<li><a href="https://www.facebook.com/MasPosibilidades/" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a href="https://plus.google.com/115331864455282109297" target="_blank"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="https://www.youtube.com/channel/UCsL_UFCakr4hmtSezJj4YbQ" target="_blank"><i class="fa fa-youtube"></i></a></li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-4 widget footer-widget from-the-blog">
				<h4><span><i class="lnr lnr-flag"></i> Links rápidos</span></h4>
				<ul class="linkrap">
					<li><a href="propiedades?ope[]=All&p=0&tipo[]=C&ord=preciomenor&in_iub=">Casas</a></li>
					<li><a href="propiedades?ope[]=All&p=0&tipo[]=D&ord=preciomenor&in_iub=">Departamentos</a></li>
					<li><a href="propiedades?ope[]=All&p=0&tipo[]=O&ord=preciomenor&in_iub=">Oficinas</a></li>
					<li><a href="propiedades?ope[]=All&p=0&tipo[]=G&ord=preciomenor&in_iub=">Galpones</a></li>
					<li><a href="propiedades?ope[]=All&p=0&tipo[]=L&ord=preciomenor&in_iub=">Locales</a></li>
					<li><a href="propiedades?ope[]=All&p=0&tipo[]=T&ord=preciomenor&in_iub=">Terrenos</a></li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-4 widget footer-widget footer-cont">
				<h4><span><i class="lnr lnr-user"></i> Contactános</span></h4>
				<p><i class="lnr lnr-map-marker"></i> Avenida Emilio Castro 7075<br/><span style="margin-left:18px">Buenos Aires</span></p>
				<p><i class="lnr lnr-envelope"></i> <a href="mailto:ventas@mpinmuebles.com.ar">ventas@mpinmuebles.com.ar</a></p>
				<p><i class="lnr lnr-phone-handset"></i> 4641 3826</p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 widget footer-widget newsletter">
				<h4><span><i class="lnr lnr-bullhorn"></i> Newsletter</span></h4>
				<p>Conocé algunas de todas las opciones que tenemos en <strong>MP Inmuebles</strong></p>
				<form class="subscribe-form" method="post" action="envio">
					<input type="hidden" name="formulario" value="newsletter"/>
					<input type="text" name="email" value="Tu e-mail..."/>
					<input type="submit" name="submit" value="Enviar" class="button small alt"/>
				</form>
			</div>
		</div>
	</div>
</footer>
<div class="bottom-bar">
	<div class="container">
	<span>Provisto por <a href="http://xintel.com.ar" target="_blank">Xintel</a><small>Perteneciente a <a href="http://amaira.com.ar" target="_blank">Amaira</a></small></span>
	</div>
</div>
<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/general.js"></script>
<script type="text/javascript">

		var $ = jQuery;
		function handleOnChange(t){
		$('#b_chosen,#b_chosen a.chosen-single').addClass('notallowed');
		//alert('aaa');
		$.ajax({
		    type:'GET',url:'barrio.php',data:'in_bar='+$(t).val()+'&get2=',
		    success:
		    	function(h){
				$('#b_chosen,#b_chosen a.chosen-single').removeClass('notallowed');
		    	$('#b').html(h);
		    	$('#b').trigger('chosen:updated');
		    }
		    });
	}
</script>
<script type="text/javascript">
function suggest(inputString){
		if(inputString.length == 0) {
			$('#suggestions').fadeOut();
		} else if(inputString.length > 2) {
		$('#campobusq').addClass('load');
			$.post("sugerencias.php", {queryString: ""+inputString+""}, function(data){
				if(data.length >0) {
					$('#suggestions').fadeIn();
					$('#suggestionsList').html(data);
					$('#campobusq').removeClass('load');
				}
			});
		}
	}
	function fill(thisValue) {
	   if(thisValue){
		$('#campobusq').val($('#'+thisValue).html());
        $('#in_iub').val(thisValue);
        }else
        {
        $('#campobusq').val('');
        $('#in_iub').val('');
        }
		$('#suggestions').fadeOut('fast');
	}
</script>
<script type="text/javascript">
$('#busven').on('click',function(){
	$('#campobusq').attr('name','');
	$('#campobusq').attr('onblur','fill();');
	$('#campobusq').attr('onkeyup','suggest(this.value);');
	$('#campobusq').attr('placeholder','Ingresa la localidad o barrio...');
	$('#ope').val('V');
});
$('#busalq').on('click',function(){
	$('#campobusq').attr('name','');
	$('#campobusq').attr('onblur','fill();');
	$('#campobusq').attr('onkeyup','suggest(this.value);');
	$('#campobusq').attr('placeholder','Ingresa la localidad o barrio...');
	$('#ope').val('A');
});
$('#codfic').on('click',function(){
	$('#campobusq').attr('name','cod');
	$('#campobusq').attr('onblur','');
	$('#campobusq').attr('onkeyup','');
	$('#campobusq').attr('placeholder','Ingresa el código de ficha. Ej: 123');
	$('#ope').val('All');
});
</script>
</body>
</html>