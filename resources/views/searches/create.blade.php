@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">

<div id='map' style="width: 100%; height: 300px; text-align:center;">
  </div>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
<form method="POST" action="/searches">

  {{ csrf_field() }}

  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
  </div>
  <div class="form-group">
    <label for="keywords">Keywords</label>
    <input type="text" class="form-control" id="keywords" name="keywords" placeholder="Keywords">
  </div>
  <div class="form-group">
    <input type="hidden" class="form-control" id="from_latitude" name="from_latitude" placeholder="from_latitude" value="-34.53">
  </div>
  <div class="form-group">
    <input type="hidden" class="form-control" id="to_latitude" name="to_latitude" placeholder="to_latitude" value="-34.65">
  </div>
  <div class="form-group">
    <input type="hidden" class="form-control" id="to_longitude" name="to_longitude" placeholder="to_longitude" value="-58.53">
  </div>
  <div class="form-group">
    <input type="hidden" class="form-control" id="from_longitude" name="from_longitude" placeholder="from_longitude" value="-58.35">
  </div>

  <input type="submit" value="Submit">
</form>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset("js/includes/searches/SearchesCreate.js") }}"></script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6GXXScjrh5oRlNsGaZKqcV1zVJS9Nt4s&callback=initMap">
</script>

@endsection