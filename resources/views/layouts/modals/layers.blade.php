@if(isset($searchSelected))
<div class="modal fade modal-layers-lg" tabindex="-1" role="dialog" aria-labelledby="modal-layers-lg" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="accordion" role="tablist">
                <div class="card">
                    <div class="card-header" role="tab" id="headingVisualizations">
                        <h5 class="mb-0">
                            <div data-toggle="collapse" href="#collapseVisualizations" aria-expanded="true" aria-controls="collapseVisualizations" style="cursor:pointer;color:#6bb23b;" class="no-seleccionable">
                                Opciones de visualización
                            </div>
                        </h5>
                    </div>
                    <div id="collapseVisualizations" class="collapse" role="tabpanel" aria-labelledby="collapseVisualizations" data-parent="#accordion">
                        <div class="card-body">
                            <div class="container-layers">
                                @include('layouts.layers.visualizations')
                            </div>
                        </div>
                    </div>
                </div>
                @foreach(App\Search::countriesLayers($searchSelected->id)->get() as $layer)
                    @if($layer->country == 'Colombia' or $layer->country == 'Argentina')
                        <div class="card">
                            <div class="card-header" role="tab" id="heading{{$layer->country}}">
                                <h5 class="mb-0">
                                    <div data-toggle="collapse" href="#collapse{{$layer->country}}" aria-expanded="true" aria-controls="collapse{{$layer->country}}" style="cursor:pointer;color:#6bb23b;" class="no-seleccionable">
                                        {{$layer->country}}
                                    </div>
                                </h5>
                            </div>
                            <div id="collapse{{$layer->country}}" class="collapse" role="tabpanel" aria-labelledby="collapse{{$layer->country}}" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="container-layers">
                                        @include("layouts.layers.layers-".strtolower($layer->country))
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif