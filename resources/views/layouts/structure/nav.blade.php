<div id="nav">
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item col-12">
                        <div id="btnProjects" onclick="location.href = '{{ route('search.index',['id'=> null] ) }}'" class="buttomGui"><img class="no-seleccionable" src="{{ asset('plugins/mine/img/png/folder.png') }}"></div>
                </li>
                @if(isset($searchSelected))
                <li class="nav-item col-12 contNavBtn">
                    <div class="buttomGui" data-toggle="modal" data-target=".modal-layers-lg"><img class="no-seleccionable" src="{{ asset('plugins/mine/img/png/stackun.png') }}"></div>
                </li>
                <li class="nav-item col-12 contNavBtn no-seleccionable" style="opacity:0;">
                    <div class="buttomGui no-seleccionable"></div>
                </li>
                @else  
                    <li class="nav-item dropdown col-12 contNavBtn" >
                        <div class="nav-link dropdown-toggle buttomGui" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="no-seleccionable" src="{{ asset('plugins/mine/img/png/earth.png') }}">
                        </div>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('search.country',['country'=> 'all'] ) }}">Todo</a>
                            <div class="dropdown-divider"></div>
                            @foreach(App\Place::Countries()->get() as $country)
                                <a class="dropdown-item" href="{{ route('search.country',['country'=> $country->country] ) }}">{{$country->country}}</a>
                            @endforeach
                        </div>
                    </li>
                <li class="nav-item col-12 contNavBtn no-seleccionable" style="opacity:0;">
                    <div class="buttomGui no-seleccionable"></div>
                </li>
                @endif
            </ul>
                
        </div>
        @if(isset($searchSelected))
        <a class="navbar-brand ml-auto no-seleccionable" style="font-size:13px;color:#6bb23b" >
            {{$searchSelected->name}}
        </a>
        @endif
        <a class="navbar-brand ml-auto" style="font-size:13px;color:#6bb23b" href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            Exit
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </nav>
</div>