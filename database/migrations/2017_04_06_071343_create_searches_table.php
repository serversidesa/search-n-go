<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searches', function(Blueprint $table) {
            $table-> increments('id');
            $table-> text('name');
            $table-> integer('status')-> default(0);
            $table-> integer('scan_status')-> default(0);
            $table-> integer('places_status')-> default(0);
            $table-> integer('contacts_status')-> default(0);
            $table-> integer('type')-> default(1);
            $table-> text('keywords')-> nullable(true);
            $table-> decimal('from_latitude', 10, 7)-> nullable(true);
            $table-> decimal('from_longitude', 10, 7)-> nullable(true);
            $table-> decimal('to_latitude', 10, 7)-> nullable(true);
            $table-> decimal('to_longitude', 10, 7)-> nullable(true);
            $table-> decimal('actual_latitude', 10, 7)-> nullable(true);
            $table-> decimal('actual_longitude', 10, 7)-> nullable(true);
            $table-> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('searches');
    }
}
