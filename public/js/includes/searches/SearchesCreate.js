      var rectangle;
      var map;
      var infoWindow;

      function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.62, lng: -58.4462},
          zoom: 9
        });

        var bounds = {
          north: -34.66,
          south: -34.53,
          east: -58.35,
          west: -58.53
        };

        // Define the rectangle and set its editable property to true.
        rectangle = new google.maps.Rectangle({
          bounds: bounds,
          editable: true,
          draggable: true
        });

        rectangle.setMap(map);

        // Add an event listener on the rectangle.
        rectangle.addListener('bounds_changed', showNewRect);

        // Define an info window on the map.
        infoWindow = new google.maps.InfoWindow();
      }
      // Show the new coordinates for the rectangle in an info window.

      /** @this {google.maps.Rectangle} */
      function showNewRect(event) {
        var ne = rectangle.getBounds().getNorthEast();
        var sw = rectangle.getBounds().getSouthWest();

        $('#from_latitude').val(ne.lat());
        $('#from_longitude').val(ne.lng());
        $('#to_latitude').val(sw.lat());
        $('#to_longitude').val(sw.lng());
      }