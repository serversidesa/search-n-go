<?php

return [
    
    'status_stopped'    		=> 0,
    'status_running'		    => 1,
    'status_paused'	    		=> 2,
    'status_completed'  		=> 3,
    'status_contacts_founded'	=> 4,
    'status_no_contacts_founded'=> 5,

    'default_radius'    => 1000,
    'default_step'      => 0.015,
    'min_average'       => 0.15,


    //'apiKey' => 'AIzaSyBpWocsDQf2oT8ndg1Elj2AmGgKdSci6TY',
    'apiKey' => 'AIzaSyC6kzM0zF66WMmln_K6KWIq9RO9aFtSAm0',

];
