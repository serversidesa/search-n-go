@extends('layouts.geobidata')

@section('content')
    
    @include('layouts.structure.nav')

    @include('layouts.structure.section')

@endsection