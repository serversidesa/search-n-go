<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function(Blueprint $table) {
            $table->increments('id');
            $table->text('place_id');
            $table->integer('search_id');
            $table->integer('status');
            $table->text('item');
            $table->text('name')-> nullable(true);
            $table->text('street_number')-> nullable(true);
            $table->text('street_name')-> nullable(true);
            $table->text('locality')-> nullable(true);
            $table->text('country')-> nullable(true);
            $table->text('postal_code')-> nullable(true);
            $table->text('phone_number')-> nullable(true);
            $table->text('international_phone_number')-> nullable(true);
            $table->text('website')-> nullable(true);
            $table->text('state')-> nullable(true);
            $table->text('administrative_area')-> nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('places');
    }
}
