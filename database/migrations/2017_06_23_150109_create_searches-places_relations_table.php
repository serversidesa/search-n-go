<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchesPlacesRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searches-places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('search_id');
            $table->integer('place_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('searches-places', function (Blueprint $table) {
            $table->drop('id');
            $table->drop('search_id');
            $table->drop('place_id');
        });
        
        Schema::drop('searches-places');
    }
}
