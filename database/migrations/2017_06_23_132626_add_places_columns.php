<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlacesColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('places', function(Blueprint $table) {
            $table->text('administrative_area_level_1')-> nullable(true);
            $table->text('administrative_area_level_2')-> nullable(true);
            $table->decimal('latitude', 10, 7)-> nullable(true);
            $table->decimal('longitude', 10, 7)-> nullable(true);
            $table->text('types')-> nullable(true);
            $table->text('sublocality')-> nullable(true);
            $table->integer('contacts_status');
            $table->integer('cuit_status');
            $table->integer('linkedin_status');
            $table->dropColumn('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
