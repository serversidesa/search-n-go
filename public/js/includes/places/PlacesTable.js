$(document).ready(function (){
    $('#PlacesTable').DataTable({
        responsive: true,

        "ajax": '{!! route('places') !!}',

        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "item" },
            { "data": "street_number" },
            { "data": "street_name" },
            { "data": "locality" },
            { "data": "country" },
            { "data": "postal_code" },
            { "data": "phone_number" },
            { "data": "website" },
            { "data": "state" },
            { "data": "administrative_area" },
        ],

        "columnDefs": [{
            "targets": [ 0 ],
            "visible": false,
        }],
    });
});