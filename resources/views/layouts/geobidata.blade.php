<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
        <link href="{{ asset('plugins/mine/css/style.css') }}" rel="stylesheet">
        
    </head>
    <body>
        <div class="loader"></div>
        @yield('content')  
        @include('layouts.modals.layers')
        
        
        <script src="{{ asset('plugins/jquery/js/jquery-3.2.1.js') }}"></script>
        <script src="{{asset('plugins/mine/js/interfaz.js') }}"></script>
        <script src="{{ asset('plugins/popper/js/popper.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
        <script src="{{ asset('plugins/google/js/markerclusterer.js') }}"> </script>
        
    </body>
</html>