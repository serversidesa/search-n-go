<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\ContactsController;
use App\Classes\GooglePlacesClient;
use App\Classes\GooglePlaces;
use App\Contact;

class Place extends Model
{
    protected $guarded = [];
    
	public function contacts() {
		return $this-> hasMany(Contact::class);
	}

    public function find_details(){
        Log::debug('['.$this-> search_id.'] ['.$this-> id.'] Finding details');
        
        $google_places = new GooglePlaces(Config::get('constants.apiKey'));
        $google_places->place_id = $this-> place_id;
        $details                 = $google_places->details();

        if ($details["status"] == 'OK') {

            $this-> name = substr($details["result"]["name"],0,90);
            $this-> latitude = $details["result"]["geometry"]["location"]["lat"];
            $this-> longitude = $details["result"]["geometry"]["location"]["lng"];

            foreach ($details['result']['address_components'] as $component) {
                if (in_array('street_number', $component['types'])) $this-> street_number = $component['long_name'];
                if (in_array('route', $component['types'])) $this-> street_name = $component['short_name'];
                if (in_array('administrative_area', $component['types'])) $this-> administrative_area = $component['long_name'];
                if (in_array('administrative_area_level_1', $component['types'])) $this-> administrative_area_level_1 = $component['long_name'];
                if (in_array('administrative_area_level_2', $component['types'])) $this-> administrative_area_level_2 = $component['long_name'];
                if (in_array('sublocality', $component['types'])) $this-> sublocality = $component['long_name'];
                if (in_array('locality', $component['types'])) $this-> locality = $component['long_name'];
                if (in_array('country', $component['types'])) $this-> country = $component['long_name'];
                if (in_array('postal_code', $component['types'])) $this-> postal_code = $component['long_name'];
                if (in_array('country', $component['types'])) $this-> country = $component['long_name'];
            }

            if (isset($details["result"]["formatted_phone_number"])) {
                $this-> phone_number = $details["result"]["formatted_phone_number"];
            }

            if (isset($details["result"]["international_phone_number"])) {
                $this-> international_phone_number = $details["result"]["international_phone_number"];
            }

            if (isset($details["result"]["website"])) {
                $this-> website = substr($details["result"]["website"],0,90);
            }

             if (isset($details["result"]["types"])) {
                foreach ($details["result"]["types"] as $type){
                    $this-> types .= $type.', ';
                }
                substr($this-> types, 0, -2);
            }

            $this-> status = Config::get('constants.status_completed');
            $this-> save();

            //Log::debug($details);
        } else {
            Log::debug('['.$this-> search_id.'] ['.$this-> id.'] Finding details');
            Log::debug('['.$this-> id.'] Detail search error: '.$details["status"]);
        }
    
        unset($google_places);
    }

    public function find_contacts() {
        if (!is_null($this-> website)){
            Log::debug('['.$this-> id.'] Finding Contacts for: '.$this-> name);

            $email_founded = $this-> scrapWebsite();

            foreach ($email_founded as $email) {
                if ($this-> validEmail($email)) {
                    if ($this-> contactExists($email)) {
                        Log::debug('Contact '.$email.' exist');
                    } else {
                        Log::debug('Contact '.$email.' NOT exist');
                        $this-> saveContact($email);
                        $this-> setContactsStatus('contacts_founded');
                    }
                }
            }

            if ($this-> isContactsStatus('contacts_founded')){
                Log::debug('['.$this-> id.'] '.$this-> countContacts().' contacts founded');
            } else{
                $this-> setContactsStatus('no_contacts_founded');
                Log::debug('['.$this-> id.'] No contacts founded');
            }
        }
    }

    protected function isStatus($status){
        if (Place::find($this-> id)-> status == Config::get('constants.status_'.$status)){
            return true;
        } else{
            return false;
        }
    }

    protected function getStatus(){
        return (Place::find($this-> id)-> status);
    }

    protected function setStatus($status){
        $this-> status = Config::get('constants.status_'.$status);
        $this-> save();
    }

    protected function isContactsStatus($status){
        if (Place::find($this-> id)-> contacts_status == Config::get('constants.status_'.$status)){
            return true;
        } else{
            return false;
        }
    }

    protected function getContactsStatus(){
        return (Place::find($this-> id)-> contacts_status);
    }

    protected function setContactsStatus($status){
        $this-> contacts_status = Config::get('constants.status_'.$status);
        $this-> save();
    }

    protected function saveContact($email){
        $newContact = new Contact;

        $newContact-> place_id = $this-> id;
        $newContact-> email = $email;

        $newContact-> save();
    }

    protected function scrapWebsite(){
        $file_name = str_replace(' ', '', $this-> name).'.txt';
        $wget_file = storage_path('app/'.$file_name);
        $domain = substr(str_replace('http://','', $this-> website), 0, -1);

        $search_emails = shell_exec('wget -4 --timeout=200 --tries=1 -l1 -q -r -A html,htm,txt,doc,php -e robots=off --domains "'.$domain.'" -O "'.$wget_file.'" "'.$this-> website.'" && cat "'.$wget_file.'" | grep -ioE "\b[a-zA-Z0-9._\-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9.-]+\b" | uniq');
            
        Storage::disk('local')-> delete($file_name);

        return explode('
', $search_emails);
    }

    protected function validEmail($email) {
        return !!filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    protected function contactExists($email){
        if (Place::find($this-> id)-> contacts-> where('email', '=', $email)-> count() >= 1){
            return true;
        } else{
            return false;
        }
    }

    protected function countContacts(){
        return Place::find($this-> id)-> contacts-> count();
    }

    public function scopePlaceByIdSearch($query, $id){
        return $query->where('searches_id',$id); 
    }
    
    public function scopeCountries($query){
        return $query->select('country')->groupBy('country');
    }    
}
